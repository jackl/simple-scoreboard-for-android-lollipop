package test.neosperience.com.thelword;

import android.graphics.Color;

/**
 * Created by omar on 04/07/14.
 */
public class PeopleDummy {
    public static enum PEOPLE_COLOR {
        BLUE(Color.parseColor("#33B5E5")),
        GREEN(Color.parseColor("#99CC00")),
        YELLOW(Color.parseColor("#FFBB33")),
        RED(Color.parseColor("#FF4444")),
        ;

        private final int mColor;

        PEOPLE_COLOR(int color) {
            mColor = color;
        }

        public int getColor() {
            return mColor;
        }
    }

    public String name;
    public PEOPLE_COLOR color;

    public PeopleDummy(String name, PEOPLE_COLOR color) {
        this.name = name;
        this.color = color;
    }
}
